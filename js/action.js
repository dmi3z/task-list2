const tasks = [
    {
        isComplete: false,
        id: 1,
        name: 'Купить хлеб'
    },
    {
        isComplete: false,
        id: 2,
        name: 'Сделать уборку'
    },
    {
        isComplete: true,
        id: 3,
        name: 'Пообедать'
    },
    {
        isComplete: false,
        id: 4,
        name: 'Выгулять собаку'
    },
    {
        isComplete: true,
        id: 5,
        name: 'Выгулять кота'
    }
];


/* function drawTask(task) {
    return `
    <div class="task">
        <input type="checkbox" class="task__complete" ${ task.isComplete ? 'checked' : '' }>
        <span class="task__number">${task.id}</span>
        <span class="task__name">${task.name}</span>
    </div>`; // task 
} */

const list = document.querySelector('.container__list');

/* for(let i = 0; i < tasks.length; i++) {
    list.innerHTML += drawTask(tasks[i]);
} */

/* tasks.forEach(item => {
    list.innerHTML += drawTask(item);
}); */

// ---- True way ---

function createTaskTag(task) {
    const name = document.createElement('span'); // name = <span></span>
    name.className = 'task__name'; // name = <span class="task__name"></span>
    name.innerText = task.name; // name = <span class="task__name">Выгулять собаку</span>

    const number = document.createElement('span');
    number.className = 'task__number';
    number.innerText = task.id;

    const checkbox = document.createElement('input'); // <input type="text">
    checkbox.type = 'checkbox'; //  <input type="checkbox">
    checkbox.className = 'task__complete';
    checkbox.checked = task.isComplete;

    const taskTag = document.createElement('div');
    taskTag.className = 'task'; // taskTag = <div class="task"></div>
    taskTag.appendChild(checkbox); // <div class="task"> <input ...>  </div>
    taskTag.appendChild(number);//  <div class="task"> <input ...> <span > </div>
    taskTag.appendChild(name);

    return taskTag;
}

tasks.forEach(item => {
    const myTask = createTaskTag(item);
    list.appendChild(myTask); // list.innerHTML += myTask;
});


